package com.example.a642.protoalarm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ListEventActivity extends AppCompatActivity {


    public int intYear;
    public int intMonth;
    public int intDay;
    public Boolean TwentyFourHour;


    public String emptyEventAM = "am";
    public String[] amEvent = {emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM,
            emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM, emptyEventAM};

    public String emptyEventPM = "pm";
    public String[] pmEvent = {emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM,
            emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM, emptyEventPM};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);

        Intent getMonthIntent = getIntent();
        Intent getDayIntent = getIntent();
        Intent getYearIntent = getIntent();
        intYear = getYearIntent.getIntExtra("intYear", 0);
        intMonth = getMonthIntent.getIntExtra("intMonth", 0);
        intDay = getDayIntent.getIntExtra("intDay",  0);
        Toast.makeText(ListEventActivity.this, intDay +"/"+ intMonth+ "/" + intYear, Toast.LENGTH_SHORT).show();
        Intent getTwentyFourHour = getIntent();
        TwentyFourHour = getTwentyFourHour.getBooleanExtra("TwentyFourHour", true);

        EventList();
        //onItemSelect();


    }

    private void EventList() {
        ListView amListView = (ListView) findViewById(R.id.listView1);


        amEvent[0] = "You should not be seeing this!!";
        amEvent[6] = "At Dawn";
        amEvent[9] = "In the Morning";
        amEvent[12] = "Midday";

        LinkedHashMap<String, String> morningEvents = new LinkedHashMap<>();

        for (int i = 0; i < 13; i++) {
            morningEvents.put(i + ".00", amEvent[i]);
        }


        List<HashMap<String, String>> listItems = new ArrayList<>();
        SimpleAdapter adapter = new SimpleAdapter(this, listItems, R.layout.event_list_item,
                new String[]{"First Line", "Second Line"},
                new int[]{R.id.text1, R.id.text2});


        Iterator it = morningEvents.entrySet().iterator();
        while (it.hasNext()) {
            HashMap<String, String> EventsMap = new HashMap<>();
            Map.Entry pair = (Map.Entry) it.next();
            EventsMap.put("First Line", pair.getKey().toString());
            EventsMap.put("Second Line", pair.getValue().toString());
            listItems.add(EventsMap);
        }


        amListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        pmEvent[3] = "In the Afternoon";
        pmEvent[6] = "In the Evening";
        pmEvent[9] = "At Night";
        pmEvent[12] = "Midnight";


        final ListView pmListView = (ListView) findViewById(R.id.listView2);

        LinkedHashMap<String, String> eventListPM = new LinkedHashMap<>();


        for (int i = 1; i < 13; i++) {
            int n = i;
            eventListPM.put(n + ".00", pmEvent[i]);
        }


        SimpleAdapter listAdapter = new SimpleAdapter(this, listItems, R.layout.event_list_item,
                new String[]{"First Line", "Second Line"},
                new int[]{R.id.text1, R.id.text2});


        Iterator iterator = eventListPM.entrySet().iterator();
        while (iterator.hasNext())

        {
            HashMap<String, String> EventsMap = new HashMap<>();
            Map.Entry pair = (Map.Entry) iterator.next();
            EventsMap.put("First Line", pair.getKey().toString());
            EventsMap.put("Second Line", pair.getValue().toString());
            listItems.add(EventsMap);
        }


        pmListView.setAdapter(listAdapter);
        adapter.notifyDataSetChanged();


        pmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    final int position, long id) {
                Intent startNewActivity = new Intent(ListEventActivity.this, EventPopupActivity.class);




                startNewActivity.putExtra("TwentyFourHour", TwentyFourHour);
                startNewActivity.putExtra("intYear", intYear);
                startNewActivity.putExtra("intMonth", intMonth);
                startNewActivity.putExtra("intDay", intDay);
                startNewActivity.putExtra("intTime", position);
                startActivity(startNewActivity);


            }
        });

    }

    @Override
    public void onBackPressed()
    {
        Intent startNewActivity = new Intent(ListEventActivity.this, CalendarAppActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_out_right);

    }

}