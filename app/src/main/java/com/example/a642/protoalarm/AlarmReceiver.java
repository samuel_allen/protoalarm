package com.example.a642.protoalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by 642 on 11/02/2017.
 */

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        String state = intent.getExtras().getString("extra");
        Log.e("MyActivity", "In the receiver with " + state);

        Intent ringtoneServiceIntent = new Intent(context, SoundAlarmService.class);
        ringtoneServiceIntent.putExtra("extra", state);

        context.startService(ringtoneServiceIntent);
    }

}


