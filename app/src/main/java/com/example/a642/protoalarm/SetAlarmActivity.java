package com.example.a642.protoalarm;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextClock;
import android.widget.TimePicker;
import android.widget.Toast;

public class SetAlarmActivity extends AppCompatActivity {

    TimePicker alarmPick;
    public java.util.Calendar myCalendar;
    int hour;
    int minute;

    PendingIntent pending_alarm;


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);


        //initiate AlarmManager
        //alarm_man = (AlarmManager) getSystemService(ALARM_SERVICE);
        //initiate TimePicker
        alarmPick = (TimePicker) findViewById(R.id.sTimePicker);
        //initiate TextClock


        //Intent for Alarm Receiver

        SaveAlarm();
        CancelAlarmButton();
        PlusButton();
        MinusButton();

    }

    private void SaveAlarm() {

        Button saveAlarmBtn = (Button) findViewById(R.id.sSaveAlarmBtn);
        saveAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // TODO *Remove redundant  Calendar.set* Keep only picker get
                final Calendar myCalendar = Calendar.getInstance();

                myCalendar.set(java.util.Calendar.HOUR_OF_DAY, alarmPick.getHour());
                myCalendar.set(java.util.Calendar.MINUTE, alarmPick.getMinute());

                int hour = alarmPick.getHour();
                int minute = alarmPick.getMinute();
                long timeInMiliSec = myCalendar.getTimeInMillis();

                //creates pending intent for alarm until user selected time

                //Create Extra "alarm on" string in intent

                Toast.makeText(SetAlarmActivity.this, "" + myCalendar.getTimeInMillis(), Toast.LENGTH_LONG)
                        .show();


                Intent startNewActivity = new Intent(SetAlarmActivity.this, AlarmConfigActivity.class);
                startNewActivity.putExtra("intHour", hour);
                startNewActivity.putExtra("intMinute", minute);
                startNewActivity.putExtra("timeInMiliSec", timeInMiliSec);
                startActivity(startNewActivity);


            }
        });

    }

    public void CancelAlarmButton() {


        Button cancelAlarmBtn = (Button) findViewById(R.id.sCancelAlarmBtn);
        cancelAlarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create Extra "alarm off" string in intent
                // alarmIntent.putExtra("Extra", "Alarm off");

                //alarm_man.cancel(pending_alarm);
                Toast.makeText(SetAlarmActivity.this, "Alarm OFF!", Toast.LENGTH_LONG)
                        .show();
            }
        });


    }

    public void PlusButton() {

        // Plus 1 minute button
        Button plusBtn = (Button) findViewById(R.id.sPlusBtn);
        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myCalendar.set(java.util.Calendar.HOUR_OF_DAY, alarmPick.getHour());
                myCalendar.set(java.util.Calendar.MINUTE, alarmPick.getMinute());

                int hour = alarmPick.getHour();
                int minute = alarmPick.getMinute();
                long timeInMiliSec = myCalendar.getTimeInMillis();

                //creates pending intent for alarm until user selected time

                //Create Extra "alarm on" string in intent

                Toast.makeText(SetAlarmActivity.this, "" + myCalendar.getTimeInMillis(), Toast.LENGTH_LONG)
                        .show();


                Intent startNewActivity = new Intent(SetAlarmActivity.this, AlarmConfigActivity.class);
                startNewActivity.putExtra("intHour", hour);
                startNewActivity.putExtra("intMinute", minute);
                startNewActivity.putExtra("timeInMiliSec", timeInMiliSec);
                startActivity(startNewActivity);


                minute = alarmPick.getMinute();
                if (minute < 59) {
                    alarmPick.setMinute(minute + 1);
                } else {
                    alarmPick.setMinute(0);
                }
                // minutePicker.setValue(alarmPick.getMinute() + 1);

            }
        });

    }


    public void MinusButton() {


        // Minus 1 minute button
        Button minusBtn = (Button) findViewById(R.id.sMinusBtn);
        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                minute = alarmPick.getMinute();
                if (minute > 0) {
                    alarmPick.setMinute(minute - 1);
                } else {
                    alarmPick.setMinute(59);
                }

            }

        });

    }
}
