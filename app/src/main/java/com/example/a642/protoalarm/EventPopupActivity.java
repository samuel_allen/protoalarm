package com.example.a642.protoalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.icu.util.Calendar;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.security.PublicKey;

import static android.R.attr.editable;
import static android.R.attr.popupKeyboard;
import static java.lang.Integer.min;
import static java.lang.Integer.parseInt;

public class EventPopupActivity extends AppCompatActivity {

    EditText description, txtMinute, txtHour;
    public int intMin, intHour, intDay, intMonth, intYear;
    public Boolean twentyFourHour = false;
    public Boolean am = true;


    public String stringDescription = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_popup);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        description = (EditText) findViewById(R.id.pDescription);

        txtMinute = (EditText) findViewById(R.id.pMinutes);
        txtHour = (EditText) findViewById(R.id.pHour);

        stringDescription = description.getText().toString();
        InputMethodManager inputMethodMan = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodMan.showSoftInput(description,0);

        MinNumPad();
        HourNumPad();
        EnterFab();
        DisplayHour();
        EnterKeyboard();
        HourOnCLickListener();
        MinuteOnCLickListener();
        SaveAlarmButton();

        MinChangedListener();
        HourChangedListener();
        onClickDescriptionListener();



        //txtMinute.setFilters(new InputFilter[]{ new TImeInputFilter(0, 59)});

        //String stringMin = txtMinute.getText().toString();
        //  int intMin = Integer.parseInt(stringMin);


        Intent getMonthIntent = getIntent();
        Intent getDayIntent = getIntent();
        Intent getYearIntent = getIntent();
        intYear = getYearIntent.getIntExtra("intYear", 0);
        intMonth = getMonthIntent.getIntExtra("intMonth", 0);
        intDay = getDayIntent.getIntExtra("intDay", 0);


        Toast.makeText(EventPopupActivity.this, intDay + "/" + intMonth + "/" + intYear,
                Toast.LENGTH_SHORT).show();


    }

    public void EnterFab() {
        final FloatingActionButton btnFab = (FloatingActionButton) findViewById(R.id.pFabEvent);
        btnFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stringDescription = description.getText().toString();
                Toast.makeText(EventPopupActivity.this, stringDescription, Toast.LENGTH_SHORT).show();

                if (stringDescription.equals("")) {

                } else {
                    //btnFab.requestFocus();

                    HideKeyboard();
                    description.clearFocus();
                }
            }
        });
    }

    public void EnterKeyboard() {
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {


                    //  EditText txtDescription =
                    //          (EditText)findViewById(R.id.pDescription);
                    stringDescription = description.getText().toString();
                    Toast.makeText(EventPopupActivity.this, stringDescription, Toast.LENGTH_SHORT).show();

                    if (stringDescription.equals("")) {
                    } else {

                        //btnFab.requestFocus();

                        HideKeyboard();
                        getWindow().getDecorView().clearFocus();
                        description.clearFocus();

                    }
                    return true;
                }
                return false;
            }
        });

    }

    public void MinChangedListener() {

        txtMinute.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Toast.makeText(EventPopupActivity.this, "before", Toast.LENGTH_SHORT).show();


            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Toast.makeText(EventPopupActivity.this, start + "on", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(EventPopupActivity.this, s +"after", Toast.LENGTH_SHORT).show();
                try {


                    intMin = Integer.parseInt(s.toString());
                    if (intMin > 59) {
                        s.replace(0, s.length(), "59");
                    }
                    if (s.length() > 1) {
                        if (!description.isFocused()) {
                            txtMinute.clearFocus();
                            txtHour.requestFocus();
                        } else {
                            txtMinute.clearFocus();
                        }
                    }
                    if (intMin < 10) {
                        FocusMinChangeListener();
                    }


                } catch (NumberFormatException e) {
                }
            }
        });
    }

    public void HourChangedListener() {

        txtHour.addTextChangedListener(new TextWatcher() {



            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Toast.makeText(EventPopupActivity.this, "before", Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Toast.makeText(EventPopupActivity.this, start + "on", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(EventPopupActivity.this, s +"after", Toast.LENGTH_SHORT).show();

                try {
                    int intMax = 12;
                    if (twentyFourHour == true) {
                        intMax = 24;
                    }
                    intHour = Integer.parseInt(s.toString());
                    if (intHour > intMax) {
                        s.replace(0, s.length(), intMax + "");
                    }
                    if (s.length() > 1) {
                        if (!description.isFocused()) {
                            txtHour.clearFocus();
                            txtMinute.requestFocus();
                        } else {
                            txtHour.clearFocus();
                        }
                    }
                    if (intHour < 10) {
                        FocusHourChangeListener();
                    }


                } catch (NumberFormatException e) {
                }
            }
        });
    }


    public void onClickDescriptionListener() {
        description.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                txtMinute.clearFocus();
                txtHour.clearFocus();

            }
        });
    }
    public void MinuteOnCLickListener(){
        txtMinute.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                InputMethodManager inputMethodMan = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodMan.showSoftInput(txtMinute,0);


            }
        });



    }


    public void HourOnCLickListener(){
        txtHour.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                InputMethodManager inputMethodMan = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodMan.showSoftInput(txtHour,0);


            }
        });



    }



    public void FocusMinChangeListener() {
        txtMinute.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!txtMinute.isFocused() && intMin < 10) {

                    // isMinFocused = false;

                    txtMinute.setText("0" + intMin, TextView.BufferType.EDITABLE);
                }

            }
        });
    }

    public void FocusHourChangeListener() {
        txtHour.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!txtHour.isFocused() && intHour < 10) {

                    // isMinFocused = false;

                    txtHour.setText("0" + intHour, TextView.BufferType.EDITABLE);
                }

            }
        });
    }

    public void HideKeyboard() {

        InputMethodManager inputMethodMan = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodMan.hideSoftInputFromWindow(description.getWindowToken(), 0);
        description.clearFocus();

    }


    public void MinNumPad(){
        txtMinute.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (txtMinute.isFocused() && intMin < 10) {

                        // isMinFocused = false;

                        txtMinute.setText("0" + intMin, TextView.BufferType.EDITABLE);

                                  }
                    return true;
                }
                return false;
            }
        });

    }

    public void HourNumPad(){
        txtHour.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (txtHour.isFocused() && intHour < 10) {

                        // isMinFocused = false;

                        txtHour.setText("0" + intHour, TextView.BufferType.EDITABLE);

                    }
                    return true;
                }
                return false;
            }
        });

    }

    public void DisplayHour(){

        Intent getSelectedIntent = getIntent();
        int selectedHour = getSelectedIntent.getIntExtra("intTime", 0);
        int displayHour = selectedHour;
        if (selectedHour > 12 && twentyFourHour == false){
            displayHour = selectedHour - 12;
            TextView textAmPm = (TextView) findViewById(R.id.pTextAmPM);
            textAmPm.setText("pm", TextView.BufferType.EDITABLE);

            am = false;

        }
            txtHour.setHint(displayHour + "");
            txtHour.setText(displayHour + "");
        if(selectedHour < 10) {
           txtHour.setHint("0" + displayHour);
           txtHour.setText("0" + displayHour);
       }


    }

    private void SaveAlarmButton() {
        Button saveAlarmBtn = (Button) findViewById(R.id.pBtnSave);

        saveAlarmBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlarmManager alarmMan;
                alarmMan = (AlarmManager) getSystemService(ALARM_SERVICE);

                Calendar alarmSetCalendar = Calendar.getInstance();

                alarmSetCalendar.set(Calendar.DAY_OF_MONTH, intDay);
                alarmSetCalendar.set(Calendar.MONTH, intMonth);
                alarmSetCalendar.set(Calendar.YEAR, intYear);
                // TODO set hour for AM/PM
                alarmSetCalendar.set(Calendar.HOUR, intHour);
                alarmSetCalendar.set(Calendar.MINUTE, intMin);
                alarmSetCalendar.set(Calendar.SECOND, 0);


                final Intent alarmIntent = new Intent(EventPopupActivity.this, AlarmReceiver.class);

                alarmIntent.putExtra("Extra", "alarm on");
                //alarmIntent.putExtra("AlarmOn", true);
                PendingIntent pendingAlarmIntent;
                pendingAlarmIntent = PendingIntent.getBroadcast(EventPopupActivity.this, 0,
                        alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                alarmMan.set(AlarmManager.RTC_WAKEUP, alarmSetCalendar.getTimeInMillis(),
                        pendingAlarmIntent);

                Toast.makeText(EventPopupActivity.this, "" + alarmSetCalendar.getTimeInMillis(), Toast.LENGTH_LONG)
                        .show();

            }
        });
    }
}

