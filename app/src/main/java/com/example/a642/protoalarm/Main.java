package com.example.a642.protoalarm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Main extends AppCompatActivity {

    public boolean TwentyFourHour = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        CalendarButton();
        AlarmActivityButton();
        WeekViewButton();
        SavedAlarmsButton();
    }

    private void AlarmActivityButton() {

        Button AlarmButton = (Button) findViewById(R.id.mAlarmBtn);
        AlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startNewActivity = new Intent(Main.this, SetAlarmActivity.class);


                startActivity(startNewActivity);
            }
        });

    }

    private void CalendarButton() {

        Button calendarButton = (Button) findViewById(R.id.mBtnCalendar);

        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent startNewActivity = new Intent(Main.this, CalendarAppActivity.class);
                startNewActivity.putExtra("TwentyFourHour", TwentyFourHour);
                startActivity(startNewActivity);

            }
        });

    }
    private void WeekViewButton(){
        Button WeekViewButton = (Button) findViewById(R.id.btnWeekView);
        WeekViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startNewActivity = new Intent(Main.this, WeekViewActivity.class);


                startActivity(startNewActivity);
            }
        });

    }




    private void SavedAlarmsButton(){
        Button SavedAlarmsButton = (Button) findViewById(R.id.mSavedAlarms);
        SavedAlarmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startNewActivity = new Intent(Main.this, SavedAlarmActivity.class);


                startActivity(startNewActivity);
            }
        });

    }



}