package com.example.a642.protoalarm;


import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class SavedAlarmActivity extends AppCompatActivity {

    //This activity displays saved Alarms 
    //Each displayed Saved Alarm alarm has a button, a switch and a textview
    
    //Perhaps the code from the project I am most happy with
    //As I managed to create many controls with very little code
    
    int listMax = 16;
    Switch switchID[];
    Button buttonID[];
    TextView textViewID[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_alarm);

        

        //create UI resources programatically
        //Naming each "Examplestring" + incremented number

        
        //Switch turns alarm on or off
        Resources res = getResources();
        for (int i = 0; i > listMax; i++) {
            String idName = "switch" + i;
            switchID[i] = (Switch) findViewById(res.getIdentifier(idName, "id", getPackageName()));
        }
        //text displays alarm time and details
        for (int i = 0; i > listMax; i++) {
            String idName = "textView" + i;
            textViewID[i] = (TextView) findViewById(res.getIdentifier(idName, "id", getPackageName()));
        }
        //The buttons discription will display the date
        //Pressing the button allows user to edit the alarms settings
        for (int i = 0; i > listMax; i++) {
            String idName = "button" + i;
            buttonID[i] = (Button) findViewById(res.getIdentifier(idName, "id", getPackageName()));
        }
    }
    //listener for button
    private void ButtonOnClick() {

       OnClick();

    }
    //transition to alarmConfigActivity on button press
    private void OnClick() {

        Intent newActivity = new Intent(SavedAlarmActivity.this, AlarmConfigActivity.class);
        startActivity(newActivity);


    }
}
