package com.example.a642.protoalarm;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

public class CalendarAppActivity extends AppCompatActivity {


    // EditText mDateEditText;
    // Calendar mCurrentDate;
    //  Bitmap mGernerateDateIcon;
    // ImageWriter mImageGenerator;
    //  ImageView mDisplayGenerateImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_app);

        final DatePicker datePicker = (DatePicker) findViewById(R.id.mDatePicker);
        final Button button = (Button) findViewById(R.id.btnSetEvent);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intYear = datePicker.getYear();
                int intMonth = datePicker.getMonth();
                int intDay =  datePicker.getDayOfMonth();
                finish();
                Intent startNewActivity = new Intent(CalendarAppActivity.this, ListEventActivity.class);

                startNewActivity.putExtra("intYear", intYear);
                startNewActivity.putExtra("intMonth", intMonth);
                startNewActivity.putExtra("intDay", intDay);
                startActivity(startNewActivity);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

            }
        });
    }
    @Override
    public void onBackPressed()
    {
        Intent startNewActivity = new Intent(CalendarAppActivity.this, Main.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_out_right);

    }

}