package com.example.a642.protoalarm;

/**
 * Created by 642 on 11/02/2017.
 */

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;

import android.os.Build;

import android.os.IBinder;

import android.util.Log;
import android.widget.Toast;

public class SoundAlarmService extends Service {

    private boolean isRunning;
    private Context context;
    MediaPlayer mediaAlarm;
    public int startId;

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.e("MyActivity", "SoundAlarmService");

        return null;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {

            mediaAlarm = MediaPlayer.create(this, R.raw.teapotwhistlemikekoenig);
            mediaAlarm.start();

        Toast.makeText(SoundAlarmService.this, "Alarm set", Toast.LENGTH_LONG)
                .show();

            this.isRunning = true;

        return START_NOT_STICKY;
    }



    @Override
    public void onDestroy() {
        Log.e("Log", "AlarmDestroy");
        super.onDestroy();

        this.isRunning = false;
    }


}