package com.example.a642.protoalarm;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

//This code is as I wrote it for my foundation year, I am very happy with it especially as I was so new
//I think the commenting is the worst thing about it.

//This class is responsible for processing user input for an alarm
//The user is presented with days of the week in check boxes


public class AlarmConfigActivity extends AppCompatActivity {

    int intHour, intMinute;
    //set check box id to ID value
    int checkBoxID[] = {R.id.checkSun,R.id.checkMon,R.id.checkTue,R.id.checkWed,R.id.checkThur,
            R.id.checkFri,R.id.checkSat,R.id.checkSun};



    CheckBox dayCheckBox;
    long timeInMilliSec;
    AlarmManager alarmMan;
    PendingIntent pendingAlarmIntent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_config);

        alarmMan = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent getMonthIntent = getIntent();
        Intent getYearIntent = getIntent();
        intHour = getYearIntent.getIntExtra("intHour", 0);
        intMinute = getMonthIntent.getIntExtra("intMinute", 0);

        SaveAlarmButton();

    }

    private void SaveAlarmButton() {
        Button saveAlarmBtn = (Button) findViewById(R.id.cSaveAlarmBtn);

        saveAlarmBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //obtain checkbox user input
                for (int i = 0; i < 7; i++) {
                    dayCheckBox = (CheckBox) findViewById(checkBoxID[i]);
                    if (dayCheckBox.isChecked()) {


                        //save alarm based on user input
                        Calendar alarmSetCalendar = Calendar.getInstance();
                        int days = Calendar.SUNDAY + (7 - alarmSetCalendar.get(Calendar.DAY_OF_WEEK)); // days until Sunday
                        alarmSetCalendar.add(Calendar.DATE, days);
                        alarmSetCalendar.set(Calendar.HOUR, 12);
                        alarmSetCalendar.set(Calendar.MINUTE, 0);
                        alarmSetCalendar.set(Calendar.SECOND, 0);
                        timeInMilliSec = alarmSetCalendar.getTimeInMillis();


                        final Intent alarmIntent = new Intent(AlarmConfigActivity.this, AlarmReceiver.class);

                        alarmIntent.putExtra("Extra", "alarm on");


                        pendingAlarmIntent = PendingIntent.getBroadcast(AlarmConfigActivity.this, 0,
                                alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                    } else {
                        //Else no specific day for alarm is selected
                        Intent getMiliSecIntent = getIntent();
                        timeInMilliSec = getMiliSecIntent.getLongExtra("timeInMilliSec", 0);

                    }
                    //Set intent for alarm date & time, then send to android service

                        final Intent alarmIntent = new Intent(AlarmConfigActivity.this, AlarmReceiver.class);

                        alarmIntent.putExtra("Extra", "alarm on");


                        pendingAlarmIntent = PendingIntent.getBroadcast(AlarmConfigActivity.this, 0,
                                alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                        alarmMan.set(AlarmManager.RTC_WAKEUP, timeInMilliSec,
                                pendingAlarmIntent);

                    //Toast message for user feed back
                    Toast.makeText(AlarmConfigActivity.this, "" + timeInMilliSec, Toast.LENGTH_LONG)
                            .show();


                    }
                }



            });
        }

    }
